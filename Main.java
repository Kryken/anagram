package com.hillel;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> words = List.of("inch", "cat", "chin", "kit", "act");
        System.out.println(getGroupedAnagrams(words));
    }

    public static int getGroupedAnagrams(List<String> words) {
        Map<String, List<String>> stringAnagramsMap = new HashMap<>();
        for (String s : words) {
            char[] arr = s.toCharArray();
            Arrays.sort(arr);
            String key = String.valueOf(arr);

            if (!stringAnagramsMap.containsKey(key))
                stringAnagramsMap.put(key, new ArrayList<>());

            stringAnagramsMap.get(key).add(s);
        }

        List<List<String>> resultList = new ArrayList<>();
        for (Map.Entry<String, List<String>> stringAnagrams : stringAnagramsMap.entrySet()) {
            resultList.add(stringAnagrams.getValue());
        }
        return resultList.size();
    }
}
